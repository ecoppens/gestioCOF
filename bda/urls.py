from django.conf.urls import url

from bda import views
from bda.views import SpectacleListView
from gestioncof.decorators import buro_required

urlpatterns = [
    url(
        r"^inscription/(?P<tirage_id>\d+)$",
        views.inscription,
        name="bda-tirage-inscription",
    ),
    url(r"^places/(?P<tirage_id>\d+)$", views.places, name="bda-places-attribuees"),
    url(r"^etat-places/(?P<tirage_id>\d+)$", views.etat_places, name="bda-etat-places"),
    url(r"^tirage/(?P<tirage_id>\d+)$", views.tirage, name="bda-tirage"),
    url(
        r"^spectacles/(?P<tirage_id>\d+)$",
        buro_required(SpectacleListView.as_view()),
        name="bda-liste-spectacles",
    ),
    url(
        r"^spectacles/(?P<tirage_id>\d+)/(?P<spectacle_id>\d+)$",
        views.spectacle,
        name="bda-spectacle",
    ),
    url(
        r"^spectacles/unpaid/(?P<tirage_id>\d+)$",
        views.UnpaidParticipants.as_view(),
        name="bda-unpaid",
    ),
    url(
        r"^spectacles/autocomplete$",
        views.spectacle_autocomplete,
        name="bda-spectacle-autocomplete",
    ),
    url(
        r"^participants/autocomplete$",
        views.participant_autocomplete,
        name="bda-participant-autocomplete",
    ),
    # Urls BdA-Revente
    url(
        r"^revente/(?P<tirage_id>\d+)/manage$",
        views.revente_manage,
        name="bda-revente-manage",
    ),
    url(
        r"^revente/(?P<tirage_id>\d+)/subscribe$",
        views.revente_subscribe,
        name="bda-revente-subscribe",
    ),
    url(
        r"^revente/(?P<tirage_id>\d+)/tirages$",
        views.revente_tirages,
        name="bda-revente-tirages",
    ),
    url(
        r"^revente/(?P<spectacle_id>\d+)/buy$",
        views.revente_buy,
        name="bda-revente-buy",
    ),
    url(
        r"^revente/(?P<revente_id>\d+)/confirm$",
        views.revente_confirm,
        name="bda-revente-confirm",
    ),
    url(
        r"^revente/(?P<tirage_id>\d+)/shotgun$",
        views.revente_shotgun,
        name="bda-revente-shotgun",
    ),
    url(r"^mails-rappel/(?P<spectacle_id>\d+)$", views.send_rappel, name="bda-rappels"),
    url(r"^catalogue/(?P<request_type>[a-z]+)$", views.catalogue, name="bda-catalogue"),
]
