from django.utils import timezone

from shared.tests.mixins import ViewTestCaseMixin

from ..models import CategorieSpectacle, Salle, Spectacle, Tirage
from .utils import create_user


class BdAViewTestCaseMixin(ViewTestCaseMixin):
    def get_users_base(self):
        return {
            "bda_other": create_user(username="bda_other"),
            "bda_member": create_user(username="bda_member", is_cof=True),
            "bda_staff": create_user(username="bda_staff", is_cof=True, is_buro=True),
        }


class BdATestHelpers:
    bda_testdata = False

    def setUp(self):
        super().setUp()

        if self.bda_testdata:
            self.load_bda_testdata()

    def load_bda_testdata(self):
        self.tirage = Tirage.objects.create(
            title="Test tirage",
            appear_catalogue=True,
            ouverture=timezone.now(),
            fermeture=timezone.now(),
        )
        self.category = CategorieSpectacle.objects.create(name="Category")
        self.location = Salle.objects.create(name="here")
        self.show1 = Spectacle.objects.create(
            title="foo",
            date=timezone.now(),
            location=self.location,
            price=0,
            slots=42,
            tirage=self.tirage,
            listing=False,
            category=self.category,
        )
        self.show2 = Spectacle.objects.create(
            title="bar",
            date=timezone.now(),
            location=self.location,
            price=1,
            slots=142,
            tirage=self.tirage,
            listing=False,
            category=self.category,
        )
        self.show3 = Spectacle.objects.create(
            title="baz",
            date=timezone.now(),
            location=self.location,
            price=2,
            slots=242,
            tirage=self.tirage,
            listing=False,
            category=self.category,
        )
