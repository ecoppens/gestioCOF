from datetime import timedelta

from django.contrib.auth.models import User
from django.utils import timezone

from ..models import CategorieSpectacle, Salle, Spectacle, Tirage


def create_user(username, is_cof=False, is_buro=False):
    user = User.objects.create_user(username=username, password=username)
    user.profile.is_cof = is_cof
    user.profile.is_buro = is_buro
    user.profile.save()
    return user


def user_is_cof(user):
    return (user is not None) and user.profile.is_cof


def user_is_staff(user):
    return (user is not None) and user.profile.is_buro


def create_spectacle(**kwargs):
    defaults = {
        "title": "Title",
        "category": CategorieSpectacle.objects.first(),
        "date": (timezone.now() + timedelta(days=7)).date(),
        "location": Salle.objects.first(),
        "price": 10.0,
        "slots": 20,
        "tirage": Tirage.objects.first(),
        "listing": False,
    }
    return Spectacle.objects.create(**dict(defaults, **kwargs))
