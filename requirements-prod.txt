-r requirements.txt

# Postgresql bindings
psycopg2<2.8

# Redis
django-redis-cache==2.1.*
redis~=2.10.6
asgi-redis==1.4.*

# ASGI protocol and HTTP server
asgiref~=1.1.2
daphne==1.3.0

# ldap bindings
python-ldap
