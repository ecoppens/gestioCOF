#!/bin/sh

set -euC

python manage.py migrate --noinput
python manage.py sync_page_translation_fields
python manage.py update_translation_fields
python manage.py loaddata gestion sites articles
python manage.py loaddevdata
