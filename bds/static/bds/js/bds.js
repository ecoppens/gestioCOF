$(function () {
    // Close notifications when delete button is pressed
    $(".notification .delete").on("click", function () {
        $(this).parent().remove();
    });

});