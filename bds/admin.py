from django.contrib import admin

from bds.models import BDSProfile

admin.site.register(BDSProfile)
