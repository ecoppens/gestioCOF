from django.urls import path

from bds import views

app_name = "bds"
urlpatterns = [
    path("", views.Home.as_view(), name="home"),
    path("autocomplete", views.BDSAutocompleteView.as_view(), name="autocomplete"),
    path("user/update/<int:pk>", views.UserUpdateView.as_view(), name="user.update"),
    path("user/create/", views.UserCreateView.as_view(), name="user.create"),
    path(
        "user/create/<slug:clipper>",
        views.UserCreateView.as_view(),
        name="user.create.fromclipper",
    ),
    path("user/delete/<int:pk>", views.UserDeleteView.as_view(), name="user.delete"),
    path("members", views.export_members, name="export.members"),
    path(
        "members/expired",
        views.ResetMembershipListView.as_view(),
        name="members.expired",
    ),
    path("members/reset", views.ResetMembershipView.as_view(), name="members.reset"),
]
