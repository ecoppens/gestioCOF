# Changelog

Liste des changements notables dans GestioCOF depuis la version 0.1 (septembre
2018).

## Le FUTUR ! (pas prêt pour la prod)

### Nouveau module de gestion des événements

- Désormais complet niveau modèles
- Export des participants implémenté

#### TODO

- Vue de création d'événements ergonomique
- Vue d'inscription à un événement **ou** intégration propre dans la vue
  "inscription d'un nouveau membre"

### Nouveau module de gestion des clubs

Uniquement un modèle simple de clubs avec des respos. Aucune gestion des
adhérents ni des cotisations.

## TODO Prod

- Créer un compte hCaptcha (https://www.hcaptcha.com/), au COF, et remplacer les secrets associés

## Version ??? - ??/??/????

## Version 0.15.1 - 15/06/2023

### K-Fêt

- Rattrape les erreurs d'envoi de mail de négatif
- Utilise l'adresse chefs pour les envois de négatifs

## Version 0.15 - 22/05/2023

### K-Fêt

- Rajoute un formulaire de contact
- Rajoute un formulaire de demande de soirée
- Désactive les mails d'envoi de négatifs sur les comptes gelés

## Version 0.14 - 19/05/2023

- Répare les dépendances en spécifiant toutes les versions

### K-Fêt

- Répare la gestion des changement d'heure via moment.js

## Version 0.13 - 19/02/2023

### K-Fêt

- Rajoute la valeur des inventaires
- Résout les problèmes de négatif ne disparaissant pas
- Affiche son surnom s'il y en a un
- Bugfixes

## Version 0.12.1 - 03/10/2022

### K-Fêt

- Fixe un problème de rendu causé par l'agrandissement du menu

## Version 0.12 - 17/06/2022

### K-Fêt

- Ajoute une exception à la limite d'historique pour les comptes `LIQ` et `#13`
- Répare le problème des étiquettes LIQ/Comptes K-Fêt inversées dans les stats des articles K-Fêt

## Version 0.11 - 26/10/2021

### COF

- Répare un problème de rendu sur le wagtail du COF

### K-Fêt

- Ajoute de mails de rappels pour les comptes en négatif
- La recherche de comptes sur K-Psul remarche normalement
- Le pointeur de la souris change de forme quand on survole un item d'autocomplétion
- Modification du gel de compte:
    - on ne peut plus geler/dégeler son compte soi-même (il faut la permission "Gérer les permissions K-Fêt")
    - on ne peut rien compter sur un compte gelé (aucune override possible), et les K-Fêteux·ses dont le compte est gelé perdent tout accès à K-Psul
    - les comptes actuellement gelés (sur l'ancien système) sont dégelés automatiquement
- Modification du fonctionnement des négatifs
    - impossible d'avoir des négatifs inférieurs à `kfet_config.overdraft_amount`
    - il n'y a plus de limite de temps sur les négatifs
    - supression des autorisations de négatif
    - il n'est plus possible de réinitialiser la durée d'un négatif en faisant puis en annulant une charge
- La gestion des erreurs passe du client au serveur, ce qui permet d'avoir des messages plus explicites
- La supression d'opérations anciennes est réparée

## Version 0.10 - 18/04/2021

### K-Fêt

- On fait sauter la limite qui empêchait de vendre plus de 24 unités d'un item à
  la fois.
- L'interface indique plus clairement quand on fait une erreur en modifiant un
  compte.
- On supprime la fonction "décalage de balance".
- L'accès à l'historique est maintenant limité à 7 jours pour raison de
  confidentialité. Les chefs/trez peuvent disposer d'une permission
  supplémentaire pour accéder à jusqu'à 30 jours en cas de problème de compta.
  L'accès à son historique personnel n'est pas limité. Les durées sont
  configurables dans `settings/cof_prod.py`.

### COF

- Le Captcha sur la page de demande de petits cours utilise maintenant hCaptcha
  au lieu de ReCaptcha, pour mieux respecter la vie privée des utilisateur·ices

## Version 0.9 - 06/02/2020

### COF / BdA

- Le COF peut remettre à zéro la liste de ses adhérents en août (sans passer par
  KDE).
- La page d'accueil affiche la date de fermeture des tirages BdA.
- On peut revendre une place dès qu'on l'a payée, plus besoin de payer toutes
  ses places pour pouvoir revendre.
- On s'assure que l'email fourni lors d'une demande de petit cours est valide.

### BDS

- Le burô peut maintenant accorder ou révoquer le statut de membre du Burô
  en modifiant le profil d'un membre du BDS.
- Le burô peut exporter la liste de ses membres avec email au format CSV depuis
  la page d'accueil.

### K-Fêt

- On affiche les articles actuellement en vente en premier lors des inventaires
  et des commandes.
- On peut supprimer un inventaire. Seuls les articles dont c'est le dernier
  inventaire sont affectés.

## Version 0.8 - 03/12/2020

### COF

- La page "Mes places" dans la section BdA indique quelles places sont sur
  listing.
- ergonomie de l'interface admin du BdA : moins d'options inutiles lors de
  la sélection de participants.
- les tirages sont maintenant archivables pour éviter d'avoir encore d'autres
  options inutiles.
- l'autocomplétion dans l'admin BdA est réparée.
- Les icones de la page de gestion des petits cours sont (à nouveau) réparées.
- On a supprimé la possibilité de modifier les mails automatiques depuis
  l'interface admin car trop problématique. Faute de mieux, envoyer un mail à
  KDE pour modifier ces mails.
- corrige un crash sporadique sur la page d'inscription au système de petits
  cours

### K-Fêt

- (fix partiel) Empêche la K-Fêt de modifier des données COF (e.g. nom, prénom,
  username) lors de la création d'un nouveau compte.
- Les statistiques de conso globales montrent deux courbes COF / non-COF au
  lieu de LIQ / sur compte.
- Un bug empêchait de fermer manuellement la K-Fêt depuis un compte non
  privilégié en tapant un mot de passe. C'est corrigé.

## Version 0.7.2 - 08/09/2020

- Nouvelle page 404
- Correction de bug en K-Fêt : le lien pour créer un nouveau compte exté apparaît
  à nouveau dans l'autocomplétion

## Version 0.7.1 - 05/09/2020

Petits ajustements sur le site du COF :

- Possibilité d'ajouter des champs d'infos supplémentaires en plus de l'email et
  de la page web dans les annuaires (clubs et partenaires).
- Corrige un bug d'affichage des adresses emails de clubs

## Version 0.7 - 29/08/2020

### GestioBDS

- Ajout d'un bouton pour supprimer un compte
- Le nombre d'adhérent⋅es est affiché sur la page d'accueil
- le groupe BDS a les bonnes permissions

### Site du COF

- Captcha fonctionnel pour les mailing-listes

### K-Fêt

- L'autocomplétion pour la création de compte K-Fêt se lance à 3 caractères seulement,
donc est plus rapide.

## Version 0.6 - 27/07/2020

Arrivée du BDS !
GestioCOF et GestioBDS ont du code en commun mais tournent de façon séparée, les
deux bases de données sont distinctes.

## Version 0.5 - 11/07/2020

### Problèmes corrigés

- La recherche d'utilisateurices (COF + K-Fêt) fonctionne de nouveau
- Bug d'affichage quand on a beaucoup de clubs dans le cadre "Accès rapide" sur
  la page des clubs (nouveau site du COF)
- Version mobile plus ergonimique sur le nouveau site du COF
- Cliquer sur "visualiser" sur les pages de clubs dans wagtail ne provoque plus
  d'erreurs 500 (nouveau site du COF)
- L'historique des ventes des articles K-Fêt fonctionne à nouveau
- Les montants en K-Fêt sont à nouveau affichés en UKF (et non en €).
- Les boutons "afficher/cacher" des mails et noms des participant⋅e⋅s à un
  spectacle BdA fonctionnent à nouveau.
- on ne peut plus compter de consos sur ☠☠☠, ni éditer les comptes spéciaux
(LIQ, GNR, ☠☠☠, #13).

### Nouvelles fonctionnalités

- On n'affiche que 4 articles sur la pages "nouveautés" (nouveau site du COF)
- Plus de traductions sur le nouveau site du COF
- Les transferts apparaissent maintenant dans l'historique K-Fêt et l'historique
  personnel.
- les statistiques K-Fêt remontent à plus d'un an (et le code est simplifié)

## Version 0.4.1 - 17/01/2020

- Corrige un bug sur K-Psul lorsqu'un trigramme contient des caractères réservés
  aux urls (\#, /...)

## Version 0.4 - 15/01/2020

- Corrige un bug d'affichage d'images sur l'interface des petits cours
- La page des transferts permet de créer un nombre illimité de transferts en
  une fois.
- Nouveau site du COF : les liens sont optionnels dans les descriptions de clubs
- Mise à jour du lien vers le calendire de la K-Fêt sur la page d'accueil
- Certaines opérations sont à nouveau accessibles depuis la session partagée
  K-Fêt.
- Le bouton "déconnexion" déconnecte vraiment du CAS pour les comptes clipper
- Corrige un crash sur la page des reventes pour les nouveaux participants.
- Corrige un bug d'affichage pour les trigrammes avec caractères spéciaux

## Version 0.3.3 - 30/11/2019

- Corrige un problème de redirection lors de la déconnexion (CAS seulement)
- Les catégories d'articles K-Fêt peuvent être exemptées de subvention COF
- Corrige un bug d'affichage dans K-Psul quand on annule une transaction sur LIQ
- Corrige une privilege escalation liée aux sessions partagées en K-Fêt
  https://git.eleves.ens.fr/klub-dev-ens/gestioCOF/issues/240

## Version 0.3.2 - 04/11/2019

- Bugfix: modifier un compte K-Fêt ne supprime plus nom/prénom

## Version 0.3.1 - 19/10/2019

- Bugfix: l'historique des utilisateurices s'affiche à nouveau

## Version 0.3 - 16/10/2019

- Comptes extés: lien pour changer son mot de passe sur la page d'accueil
- Les utilisateurices non-COF peuvent éditer leur profil
- Un peu de pub pour KDEns sur la page d'accueil
- Fix erreur 500 sur /bda/revente/<tirage_id>/manage
- Si on essaie d'accéder au compte que qqn d'autre on a une 404 (et plus une 403)
- On ne peut plus modifier des comptes COF depuis l'interface K-Fêt
- Le champ de paiement BdA se fait au niveau des attributions
- Affiche un message d'erreur plutôt que de crasher si échec de l'envoi du mail
  de bienvenue aux nouveaux membres
- On peut supprimer des comptes et des articles K-Fêt
- Passage à Django2
- Dev : on peut désactiver la barre de debug avec une variable shell
- Remplace les CSS de Google par des polices de proximité
- Passage du site du COF et de la K-Fêt en Wagtail 2.3 et Wagtail-modeltranslation 0.9
- Ajoute un lien vers l'administration générale depuis les petits cours
- Abandon de l'ancien catalogue BdA (déjà plus utilisé depuis longtemps)
- Force l'unicité des logins clipper
- Nouveau site du COF en wagtail
- Meilleurs affichage des longues listes de spectacles à cocher dans BdA-Revente
- Bugfix : les pages de la revente ne sont plus accessibles qu'aux membres du
  COF

## Version 0.2 - 07/11/2018

- Corrections de bugs d'interface dans l'inscription aux tirages BdA
- On peut annuler une revente à tout moment
- Pleiiiiin de tests

## Version 0.1 - 09/09/2018

Début de la numérotation des versions, début du changelog
