from django.urls import path

from gestioncof.decorators import buro_required
from petitscours import views
from petitscours.views import DemandeDetailView, DemandeListView

urlpatterns = [
    path("inscription", views.inscription, name="petits-cours-inscription"),
    path("demande", views.demande, name="petits-cours-demande"),
    path(
        "demande-raw",
        views.demande,
        kwargs={"raw": True},
        name="petits-cours-demande-raw",
    ),
    path(
        "demandes",
        buro_required(DemandeListView.as_view()),
        name="petits-cours-demandes-list",
    ),
    path(
        "demandes/<int:pk>",
        buro_required(DemandeDetailView.as_view()),
        name="petits-cours-demande-details",
    ),
    path(
        "demandes/<int:demande_id>/traitement",
        views.traitement,
        name="petits-cours-demande-traitement",
    ),
    path(
        "demandes/<int:demande_id>/retraitement",
        views.traitement,
        kwargs={"redo": True},
        name="petits-cours-demande-retraitement",
    ),
]
