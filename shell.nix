{ pkgs ? import <nixpkgs> { }, ... }:

let
  python = pkgs.python38;

  django-types = python.pkgs.buildPythonPackage rec {
    pname = "django-types";
    version = "0.17.0";

    format = "pyproject";

    src = pkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-wcQqt4h2xXxyg0LVqwYHJas3H8jcg7uFuuC+BoRqrXA=";
    };

    nativeBuildInputs = with python.pkgs; [ poetry-core ];

    # setup.cfg tries to pull in nonexistent LICENSE.txt file
    # postPatch = "rm setup.cfg";

    # propagatedBuildInputs = [ django typing-extensions ];
  };
in


pkgs.mkShell {
  shellHook = ''
    export DJANGO_SETTINGS_MODULE=gestioasso.settings.local

    virtualenv .venv
    source .venv/bin/activate

    pip install -r requirements-devel.txt | grep -v 'Requirement already satisfied:'
  '';

  packages = [ python django-types ] ++ (with python.pkgs; [
    pip
    virtualenv
    python-ldap
  ]);

  allowSubstitutes = false;
}
