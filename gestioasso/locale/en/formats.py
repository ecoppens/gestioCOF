# -*- encoding: utf-8 -*-

"""
English formatting.
"""

from __future__ import unicode_literals

DATETIME_FORMAT = r"l N j, Y \a\t P"
DATE_FORMAT = r"l N j, Y"
TIME_FORMAT = r"P"
