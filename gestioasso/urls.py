"""
Fichier principal de configuration des urls du projet GestioCOF
"""
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import RedirectView

bds_is_alone = (
    "bds" in settings.INSTALLED_APPS and "gestioncof" not in settings.INSTALLED_APPS
)

admin.autodiscover()
urlpatterns = [
    # Website administration (independent from installed apps)
    path("admin/doc/", include("django.contrib.admindocs.urls")),
    path("admin/", admin.site.urls),
]

if not bds_is_alone:
    # Redirection / → /gestion, only useful for developpers.
    urlpatterns.append(path("", RedirectView.as_view(url="gestion/")))

# App-specific urls

app_dict = {
    "bds": "" if bds_is_alone else "bds/",
    "kfet": "k-fet/",
    # Below = GestioCOF → goes under gestion/
    "gestioncof": "gestion/",
    "bda": "gestion/bda/",
    "petitscours": "gestion/petitcours/",
    "events": "gestion/event_v2/",  # the events module is still experimental !
    "authens": "gestion/authens/",
}
for app_name, url_prefix in app_dict.items():
    if app_name in settings.INSTALLED_APPS:
        urlpatterns += [path(url_prefix, include("{}.urls".format(app_name)))]


if "django_js_reverse" in settings.INSTALLED_APPS:
    from django_js_reverse.views import urls_js

    urlpatterns += [
        path("jsreverse/", urls_js, name="js_reverse"),
    ]

if "debug_toolbar" in settings.INSTALLED_APPS:
    import debug_toolbar

    urlpatterns += [path("__debug__/", include(debug_toolbar.urls))]

if settings.DEBUG:
    # Si on est en production, MEDIA_ROOT est servi par Apache.
    # Il faut dire à Django de servir MEDIA_ROOT lui-même en développement.
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# Wagtail URLs (wagtail.core urls must be last, as catch-all)
if "wagtail.core" in settings.INSTALLED_APPS:
    from wagtail.admin import urls as wagtailadmin_urls
    from wagtail.core import urls as wagtail_urls
    from wagtail.documents import urls as wagtaildocs_urls

    urlpatterns += [
        path("cms/", include(wagtailadmin_urls)),
        path("documents/", include(wagtaildocs_urls)),
    ]
    urlpatterns += i18n_patterns(
        path("", include(wagtail_urls)), prefix_default_language=False
    )
