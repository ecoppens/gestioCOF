import datetime
from unittest import mock

from django.test import TestCase
from django.utils import timezone

from kfet.forms import KPsulCheckoutForm
from kfet.models import Checkout

from .utils import create_user


class KPsulCheckoutFormTests(TestCase):
    def setUp(self):
        self.now = timezone.now()

        user = create_user()

        self.c1 = Checkout.objects.create(
            name="C1",
            balance=10,
            created_by=user.profile.account_kfet,
            valid_from=self.now,
            valid_to=self.now + datetime.timedelta(days=1),
        )

        self.form = KPsulCheckoutForm()

    def test_checkout(self):
        checkout_f = self.form.fields["checkout"]
        self.assertListEqual(
            list(checkout_f.choices), [("", "---------"), (self.c1.pk, "C1")]
        )

    @mock.patch("django.utils.timezone.now")
    def test_checkout_valid(self, mock_now):
        """
        Checkout are filtered using the current datetime.
        Regression test for #184.
        """
        self.now += datetime.timedelta(days=2)
        mock_now.return_value = self.now

        form = KPsulCheckoutForm()

        checkout_f = form.fields["checkout"]
        self.assertListEqual(list(checkout_f.choices), [("", "---------")])
