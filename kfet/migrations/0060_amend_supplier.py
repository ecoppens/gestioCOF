# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("kfet", "0059_create_generic")]

    operations = [
        migrations.AlterField(
            model_name="supplier",
            name="address",
            field=models.TextField(verbose_name="adresse", blank=True),
        ),
        migrations.AlterField(
            model_name="supplier",
            name="articles",
            field=models.ManyToManyField(
                verbose_name="articles vendus",
                through="kfet.SupplierArticle",
                related_name="suppliers",
                to="kfet.Article",
            ),
        ),
        migrations.AlterField(
            model_name="supplier",
            name="comment",
            field=models.TextField(verbose_name="commentaire", blank=True),
        ),
        migrations.AlterField(
            model_name="supplier",
            name="email",
            field=models.EmailField(
                max_length=254, verbose_name="adresse mail", blank=True
            ),
        ),
        migrations.AlterField(
            model_name="supplier",
            name="phone",
            field=models.CharField(max_length=20, verbose_name="téléphone", blank=True),
        ),
    ]
