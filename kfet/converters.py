"""
Les converters sont la méthode "clean" de faire les regex custom dans des URLs django.
Plus d'info dans la doc :
https://docs.djangoproject.com/en/2.2/topics/http/urls/#registering-custom-path-converters
"""


class TrigrammeConverter:
    regex = ".{3}"

    def to_python(self, value):
        return str(value)

    def to_url(self, value):
        return str(value)
