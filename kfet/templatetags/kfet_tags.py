from django import template

from ..utils import to_ukf

register = template.Library()

register.filter("ukf", to_ukf)


@register.filter()
def widget_type(field):
    return field.field.widget.__class__.__name__


@register.filter()
def slice(t, start, end=None):
    if end is None:
        end = start
        start = 0
    return t[start:end]
