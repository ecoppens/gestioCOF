from modeltranslation.decorators import register
from modeltranslation.translator import TranslationOptions

from .models import KFetPage


@register(KFetPage)
class KFetPageTr(TranslationOptions):
    fields = []
