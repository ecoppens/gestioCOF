var Account = Backbone.Model.extend({

    defaults: {
        'id': 0,
        'name': '',
        'email': '',
        'is_cof': '',
        'promo': '',
        'balance': '',
        'is_frozen': false,
        'departement': '',
        'nickname': '',
    },

    url: function () {
        return django_urls["kfet.account.read.json"](encodeURIComponent(this.get("trigramme")))
    },

    reset: function () {
        // Réinitialise les attributs du modèle à leurs défaults, sauf le trigramme qui est bind à l'input.
        // On n'utilise pas .clear() car on ne veut pas clear le trigramme.
        this.set(this.defaults)
    },

    parse: function (resp, options) {
        if (_.has(resp, 'balance')) {
            return _.extend(resp, { 'balance': parseFloat(resp.balance) });
        } else {
            return resp;
        }
    },

    view: function () {
        if (this.is_empty_account()) {
            view_class = EmptyAccountView
        } else if (this.get("trigramme") == 'LIQ') {
            view_class = LIQView
        } else {
            view_class = AccountView
        }
        return new view_class({ model: this })
    },

    render: function () {
        this.view().render();
    },

    is_empty_account: function () {
        return (this.id == 0)
    },
})

var AccountView = Backbone.View.extend({

    el: '#account',
    buttons: '.buttons',
    id_field: "#id_on_acc",

    props: _.keys(Account.prototype.defaults),

    get: function (property) {
        /* If the function this.get_<property> is defined,
        we call it ; else we call this.model.<property>. */
        getter_name = `get_${property}`;
        if (_.functions(this).includes(getter_name))
            return this[getter_name]()
        else
            return this.model.get(property)
    },

    get_is_cof: function () {
        return this.model.get("is_cof") ? 'COF' : 'Non-COF';
    },

    get_balance: function () {
        return amountToUKF(this.model.get("balance"), this.model.get("is_cof"), true);
    },

    attr_data_balance: function () {
        // Cette fonction est utilisée uniquement sur un compte valide

        if (this.model.get("is_frozen")) {
            return "frozen";
        } else if (this.model.get("balance") < 0) {
            return 'neg';
        } else if (this.model.get("balance") <= 5) {
            return 'low';
        } else {
            return 'ok';
        }
    },

    get_buttons: function () {
        var url = django_urls["kfet.account.read"](this.model.get("trigramme"));

        return `<a href="${url}" class="btn btn-primary" target="_blank" title="Modifier"><span class="glyphicon glyphicon-cog"></span></a>`;
    },

    render: function () {
        for (let prop of this.props) {
            var selector = "#account-" + prop;
            this.$(selector).text(this.get(prop));
        }

        this.$el.attr("data-balance", this.attr_data_balance());
        this.$(this.buttons).html(this.get_buttons());
        $(this.id_field).val(this.get("id"));
    },
})

var LIQView = AccountView.extend({
    get_balance: function () {
        return "";
    },

    attr_data_balance: function () {
        return 'ok';
    }
})

var EmptyAccountView = AccountView.extend({
    get: function () {
        return '';
    },

    attr_data_balance: function () {
        return '';
    },

    get_buttons: function () {
        /* Léger changement de fonctionnement :
        on affiche *toujours* le bouton de recherche si
        le compte est invalide */
        buttons = '<button class="btn btn-primary search" title="Rechercher"><span class="glyphicon glyphicon-search"></span></button>';
        trigramme = this.model.get("trigramme")
        if (trigramme.is_valid_trigramme()) {
            trigramme = encodeURIComponent(trigramme);
            var url_base = django_urls["kfet.account.create"]();
            var url = `${url_base}?trigramme=${trigramme}`;
            buttons += `<a href="${url}" class="btn btn-primary" target="_blank" title="Créer"><span class="glyphicon glyphicon-plus"></span></a>`;
        }

        return buttons
    }
})