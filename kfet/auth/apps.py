from django.apps import AppConfig
from django.db.models.signals import post_migrate
from django.utils.translation import ugettext_lazy as _


class KFetAuthConfig(AppConfig):
    name = "kfet.auth"
    label = "kfetauth"
    verbose_name = _("K-Fêt - Authentification et Autorisation")

    def ready(self):
        from . import signals  # noqa
        from .utils import setup_kfet_generic_user

        post_migrate.connect(setup_kfet_generic_user, sender=self)
