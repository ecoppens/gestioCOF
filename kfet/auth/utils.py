import hashlib

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission

from kfet.models import Account

User = get_user_model()


def get_kfet_generic_user():
    """
    Return the user related to the kfet generic account.
    """
    return Account.objects.get_generic().user


def setup_kfet_generic_user(**kwargs):
    """
    First steps of setup of the kfet generic user are done in a migration, as
    it is more robust against database schema changes.
    Following steps cannot be done from migration.
    """
    generic = get_kfet_generic_user()
    generic.user_permissions.add(
        Permission.objects.get(content_type__app_label="kfet", codename="is_team")
    )


def hash_password(password):
    return hashlib.sha256(password.encode("utf-8")).hexdigest()
