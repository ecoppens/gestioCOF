from django.contrib.auth.context_processors import PermWrapper


def temporary_auth(request):
    if hasattr(request, "real_user"):
        return {"user": request.real_user, "perms": PermWrapper(request.real_user)}
    return {}
