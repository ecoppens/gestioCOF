from django.forms.utils import flatatt
from django.forms.widgets import Widget
from django.utils.safestring import mark_safe


class TriStateCheckbox(Widget):
    def __init__(self, attrs=None, choices=()):
        super().__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render(self, name, value, attrs=None, choices=(), renderer=None):
        if value is None:
            value = "none"
        attrs["value"] = value
        final_attrs = self.build_attrs(self.attrs, attrs)
        output = ['<span class="tristate"%s></span>' % flatatt(final_attrs)]
        return mark_safe("\n".join(output))
