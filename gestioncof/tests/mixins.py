from gestioncof.models import Event
from shared.tests.mixins import (
    CSVResponseMixin,
    ViewTestCaseMixin as BaseViewTestCaseMixin,
)

from .utils import create_member, create_staff, create_user


class MegaHelperMixin(CSVResponseMixin):
    """
    Mixin pour aider aux tests du MEGA: création de l'event et de plusieurs
    inscriptions, avec options et commentaires.
    """

    def setUp(self):
        super().setUp()

        u1 = create_user("u1")
        u1.first_name = "first"
        u1.last_name = "last"
        u1.email = "user@mail.net"
        u1.save()
        u1.profile.phone = "0123456789"
        u1.profile.departement = "Dept"
        u1.profile.comments = "profile.comments"
        u1.profile.save()

        u2 = create_user("u2")
        u2.profile.save()

        m = Event.objects.create(title="MEGA 2018")

        cf1 = m.commentfields.create(name="Commentaires")
        cf2 = m.commentfields.create(name="Comment Field 2", fieldtype="char")

        option_type = m.options.create(name="Orga ? Conscrit ?")
        choice_orga = option_type.choices.create(value="Orga")
        choice_conscrit = option_type.choices.create(value="Conscrit")

        mr1 = m.eventregistration_set.create(user=u1)
        mr1.options.add(choice_orga)
        mr1.comments.create(commentfield=cf1, content="Comment 1")
        mr1.comments.create(commentfield=cf2, content="Comment 2")

        mr2 = m.eventregistration_set.create(user=u2)
        mr2.options.add(choice_conscrit)

        self.u1 = u1
        self.u2 = u2
        self.m = m
        self.choice_orga = choice_orga
        self.choice_conscrit = choice_conscrit


class ViewTestCaseMixin(BaseViewTestCaseMixin):
    """
    TestCase extension to ease testing of cof views.

    Most information can be found in the base parent class doc.
    This class performs some changes to users management, detailed below.

    During setup, three users are created:
    - 'user': a basic user without any permission,
    - 'member': (profile.is_cof is True),
    - 'staff': (profile.is_cof is True) && (profile.is_buro is True).
    """

    def get_users_base(self):
        return {
            "user": create_user("user"),
            "member": create_member("member"),
            "staff": create_staff("staff"),
        }
