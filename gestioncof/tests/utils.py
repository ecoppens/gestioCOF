from django.contrib.auth import get_user_model

User = get_user_model()


def _create_user(username, is_cof=False, is_staff=False, attrs=None):
    if attrs is None:
        attrs = {}

    password = attrs.pop("password", username)

    user_keys = ["first_name", "last_name", "email", "is_staff", "is_superuser"]
    user_attrs = {k: v for k, v in attrs.items() if k in user_keys}

    profile_keys = [
        "is_cof",
        "login_clipper",
        "phone",
        "occupation",
        "departement",
        "type_cotiz",
        "mailing_cof",
        "mailing_bda",
        "mailing_bda_revente",
        "comments",
        "is_buro",
        "petit_cours_accept",
        "petit_cours_remarques",
    ]
    profile_attrs = {k: v for k, v in attrs.items() if k in profile_keys}

    if is_cof:
        profile_attrs["is_cof"] = True

    if is_staff:
        # At the moment, admin is accessible by COF staff.
        user_attrs["is_staff"] = True
        profile_attrs["is_buro"] = True

    user = User(username=username, **user_attrs)
    user.set_password(password)
    user.save()

    for k, v in profile_attrs.items():
        setattr(user.profile, k, v)
    user.profile.save()

    return user


def create_user(username, attrs=None):
    return _create_user(username, attrs=attrs)


def create_member(username, attrs=None):
    return _create_user(username, is_cof=True, attrs=attrs)


def create_staff(username, attrs=None):
    return _create_user(username, is_cof=True, is_staff=True, attrs=attrs)


def create_root(username, attrs=None):
    if attrs is None:
        attrs = {}
    attrs.setdefault("is_staff", True)
    attrs.setdefault("is_superuser", True)
    return _create_user(username, attrs=attrs)
