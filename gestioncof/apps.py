from django.apps import AppConfig


class GestioncofConfig(AppConfig):
    name = "gestioncof"
    verbose_name = "Gestion des adhérents du COF"

    def ready(self):
        from . import signals  # noqa

        self.register_config()

    def register_config(self):
        import djconfig

        from .forms import GestioncofConfigForm

        djconfig.register(GestioncofConfigForm)
