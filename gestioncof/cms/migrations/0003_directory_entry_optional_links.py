# Generated by Django 2.2.8 on 2019-12-20 16:22

import wagtail.core.blocks
import wagtail.core.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("cofcms", "0002_auto_20190523_1521"),
    ]

    operations = [
        migrations.AlterField(
            model_name="cofdirectoryentrypage",
            name="links",
            field=wagtail.core.fields.StreamField(
                [
                    (
                        "lien",
                        wagtail.core.blocks.StructBlock(
                            [
                                ("url", wagtail.core.blocks.URLBlock(required=True)),
                                ("texte", wagtail.core.blocks.CharBlock()),
                            ]
                        ),
                    ),
                    (
                        "contact",
                        wagtail.core.blocks.StructBlock(
                            [
                                (
                                    "email",
                                    wagtail.core.blocks.EmailBlock(required=True),
                                ),
                                ("texte", wagtail.core.blocks.CharBlock()),
                            ]
                        ),
                    ),
                ],
                blank=True,
            ),
        ),
        migrations.AlterField(
            model_name="cofdirectoryentrypage",
            name="links_en",
            field=wagtail.core.fields.StreamField(
                [
                    (
                        "lien",
                        wagtail.core.blocks.StructBlock(
                            [
                                ("url", wagtail.core.blocks.URLBlock(required=True)),
                                ("texte", wagtail.core.blocks.CharBlock()),
                            ]
                        ),
                    ),
                    (
                        "contact",
                        wagtail.core.blocks.StructBlock(
                            [
                                (
                                    "email",
                                    wagtail.core.blocks.EmailBlock(required=True),
                                ),
                                ("texte", wagtail.core.blocks.CharBlock()),
                            ]
                        ),
                    ),
                ],
                blank=True,
                null=True,
            ),
        ),
        migrations.AlterField(
            model_name="cofdirectoryentrypage",
            name="links_fr",
            field=wagtail.core.fields.StreamField(
                [
                    (
                        "lien",
                        wagtail.core.blocks.StructBlock(
                            [
                                ("url", wagtail.core.blocks.URLBlock(required=True)),
                                ("texte", wagtail.core.blocks.CharBlock()),
                            ]
                        ),
                    ),
                    (
                        "contact",
                        wagtail.core.blocks.StructBlock(
                            [
                                (
                                    "email",
                                    wagtail.core.blocks.EmailBlock(required=True),
                                ),
                                ("texte", wagtail.core.blocks.CharBlock()),
                            ]
                        ),
                    ),
                ],
                blank=True,
                null=True,
            ),
        ),
    ]
