$(function(){
    var mem = {};
    var ctt = $("#calendar-wrap");
    makeInteractive();
    
    function makeInteractive () {
        $(".cal-btn").on("click", loadCalendar);
        $(".hasevent a").on("click", showEvents);
    }

    function loadCalendar () {
        var url = $(this).attr("cal-dest");
        if (mem[url] != undefined) {
            ctt.html(mem[url]);
            makeInteractive();
            return;
        }
        ctt.innerText = "Chargement...";
        ctt.load(url, function () {
            mem[url] = this.innerHTML;
            makeInteractive();
        });
    }

    function showEvents() {
        ctt.find(".details").remove();
        ctt.append(
            $("<div>", {class:"details"})
                .html(this.nextElementSibling.outerHTML)
        );
    }
});
