import re

from django import forms
from django.utils.translation import gettext as _


class CaptchaForm(forms.Form):
    answer = forms.CharField(label="Réponse", max_length=32)

    def clean_answer(self):
        value = self.cleaned_data["answer"]
        if not re.match(r"(les|the)? *ernests?", value.strip().lower()):
            raise forms.ValidationError(_("Réponse incorrecte"))

        return value
