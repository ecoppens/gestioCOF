# Generated by Django 2.2.28 on 2023-05-22 09:01

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("gestioncof", "0018_petitscours_email"),
    ]

    operations = [
        migrations.AddField(
            model_name="cofprofile",
            name="date_adhesion",
            field=models.DateField(
                blank=True, null=True, verbose_name="Date d'adhésion"
            ),
        ),
    ]
